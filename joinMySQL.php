<?php

// Left Join 
$query_left_join = "SELECT `people`.`name`, `pets`.`pet` FROM `people` LEFT JOIN `people`.`id` = `pets`.`people_id`";



// Right Join 
$query_right_join = "SELECT `people`.`name`, `pets`.`pet` FROM `people` RIGHT JOIN `people`.`id` = `pets`.`people_id`";



// Normal Join 
$query_join = "SELECT `people`.`name`, `pets`.`pet` FROM `people` JOIN `people`.`id` = `pets`.`people_id`";


