<?php
session_start();

require 'class/Shortener.php';

$s = new Shortener();

if(isset($_POST['url'])) {
    $url = $_POST['url'];
    
    if($code = $s->makeCode($url)) {
        $_SESSION['feedback'] = "Generated! Your short URL is: <a href=\"http://localhost/URLShorter/{$code}\">http://localhost/URLShorter/{$code}</a>";
    } else {
        $_SESSION['feedback'] = "There was a problem. Invalid URL, Perhaps?";
    }
}

header('Location: index.php');