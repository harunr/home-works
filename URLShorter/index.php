<?php
session_start();
?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>URL Shortener</title>
    <link rel="stylesheet" href="css/global.css" />
</head>
<body>
    <div class="container">
        <h1 class="title">Shorten an URL Here</h1>
        
        <?php 
            if(isset($_SESSION['feedback'])){
                
                echo "<p>{$_SESSION['feedback']}</p>";
                unset($_SESSION['feedback']);
            }
        ?>
        <form action="shorten.php" method="POST">
            <input type="url" name="url" placeholder="Enter an URL" autocomplete="off" />
            <input type="submit" value="Shorten" />
        </form>
    </div>
</body>
</html>

