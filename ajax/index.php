<?php

// AJAX = Asynchronous JavaScript and XML

?>
<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>This is title</title>
        <script type="text/javascript">
            function load (thediv, thefile){
               if(window.XMLHttpRequest){
                  xmlhttp = new XMLHttpRequest(); 
               }else{
                   xmlhttp = new ActiveXObject('MicrosoftXMLHTTP');
               }
               xmlhttp.onreadystatechange = function(){
                   if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                       document.getElementById(thediv).innerHTML = xmlhttp.responsetext;
                       
                   }
               }
               
               xmlhttp.open('GET', thefile, true);
               xmlhttp.send();
            }
        </script>
    </head>
    <body>
        <input type="submit" value="Submit" onclick="load('adiv', 'includ.inc.php');" />
        <div id="adiv"></div>
    </body>
</html>