<?php

class BankAccount {
    public $balance = 0;
    
    public function DisplayBalance() {
        return 'Balance: '.$this->balance;
    }
    
    public function Withdraw($amount) {
        if($this->balance<$amount){
            echo 'Not enough money <br />';
        } else {
            $this->balance = $this->balance - $amount;
        }
    }
    
    public function Deposite($amount) {
        $this->balance = $this->balance + $amount;
    }
     
}



// New instance of class
$rayhan = new BankAccount();
$rayhan->Deposite(1000);
$rayhan->Withdraw(245);
echo $rayhan->DisplayBalance();

$king = new BankAccount();

class SavingsAccount extends BankAccount {
    public $type = '18-25';
    
}